"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var startup_1 = __importDefault(require("./startup"));
var PORT = process.env.PORT || 8085;
startup_1.default.listen(PORT, function (err) {
    if (err) {
        console.log(err);
    }
    console.log("Server Runnings on: http://localhost:" + PORT + "/api/v1/employees");
});
//# sourceMappingURL=index.js.map