"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var inversify_1 = require("inversify");
var EmployeeRepository_1 = __importDefault(require("../infrastructure/repositories/EmployeeRepository"));
var EmployeeService_1 = __importDefault(require("../domain/services/EmployeeService"));
var InversifyConfig = /** @class */ (function () {
    function InversifyConfig() {
    }
    InversifyConfig.Init = function () {
        var container = new inversify_1.Container();
        container.bind("IEmployeeRepository").to(EmployeeRepository_1.default).inTransientScope();
        container.bind("IEmployeeService").to(EmployeeService_1.default).inTransientScope();
        return container;
    };
    return InversifyConfig;
}());
exports.default = InversifyConfig;
//# sourceMappingURL=InversifyConfig.js.map