"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var EmployeeController_1 = __importDefault(require("../controllers/EmployeeController"));
var EmployeeRoutes = /** @class */ (function () {
    function EmployeeRoutes() {
        this.employeeController = new EmployeeController_1.default();
        this.router = express_1.Router();
        this.router.get('/', this.employeeController.GetEmployees);
    }
    return EmployeeRoutes;
}());
exports.default = new EmployeeRoutes();
//# sourceMappingURL=EmployeeRoutes.js.map