"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var inversify_1 = require("../../node_modules/inversify");
require("reflect-metadata");
var EmployeeRepository = /** @class */ (function () {
    function EmployeeRepository() {
    }
    EmployeeRepository.prototype.EmployeeRepository = function () { };
    EmployeeRepository.prototype.GetAllEmployee = function () {
        var employees = new Array({
            Name: "Alexis",
            LastName: "Mateo",
            Age: 47,
            Id: 1
        }, {
            Name: "Alexis",
            LastName: "Mateo",
            Age: 47,
            Id: 1
        }, {
            Name: "Alexis",
            LastName: "Mateo",
            Age: 47,
            Id: 1
        }, {
            Name: "Alexis",
            LastName: "Mateo",
            Age: 47,
            Id: 1
        });
        return employees;
    };
    EmployeeRepository = __decorate([
        inversify_1.injectable()
    ], EmployeeRepository);
    return EmployeeRepository;
}());
exports.default = EmployeeRepository;
//# sourceMappingURL=EmployeeRepository.js.map