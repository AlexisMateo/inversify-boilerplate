"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var typeorm_1 = require("typeorm");
var path_1 = __importDefault(require("path"));
var typedi_1 = require("typedi");
var connectionConfig = {
    name: 'default',
    type: 'mysql',
    host: "localhost",
    username: "root",
    password: "admin",
    database: "mobile-store",
    insecureAuth: true,
    port: 3306,
    ssl: true,
    logging: ['query', 'error'],
    extra: { "insecureAuth": true },
    entities: [path_1.default.join(__dirname, '/domain/entities/*')]
};
typeorm_1.useContainer(typedi_1.Container);
typeorm_1.createConnection(connectionConfig);
//# sourceMappingURL=mysql.js.map