import {Router} from 'express';
import EmployeeController from '../../api/controllers/EmployeeController';


 class EmployeeRoutes { 

    public employeeController: EmployeeController = new EmployeeController();
    public router = Router();

    constructor(){
        this.router.get('/', this.employeeController.GetEmployees);
    }

}

export default new EmployeeRoutes();