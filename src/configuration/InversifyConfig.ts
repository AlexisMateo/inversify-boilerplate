import { Container } from "inversify";
import IEmployeeRepository from "../infrastructure/repositories/IEmployeeRepository";
import IEmployeeService from "../domain/services/IEmployeeService";
import EmployeeRepository from "../infrastructure/repositories/EmployeeRepository";
import EmployeeService from "../domain/services/EmployeeService";

export default class InversifyConfig{

    public static Init():Container{
        
        let container :Container = new Container();

        container.bind<IEmployeeRepository>("IEmployeeRepository").to(EmployeeRepository).inTransientScope();
        container.bind<IEmployeeService>("IEmployeeService").to(EmployeeService).inTransientScope();

        return container;
    } 

}