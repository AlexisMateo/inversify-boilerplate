import { Entity, Column, PrimaryGeneratedColumn, Generated } from 'typeorm'
import { Length, IsEmail } from 'class-validator'

export interface UserProps {
    email: string
    password: string
}

@Entity('user')
export class User {
    @PrimaryGeneratedColumn()
    id: number | undefined

    @Column({ unique: true })
    @IsEmail(undefined, {
        message: "NOT_VALID_EMAIL"
    })
    email: string

    @Column({ select: false, length: 64 })
    @Length(6, 64, {
        message: "PASSWORD_NOT_SECURE"
    })
    password: string

    constructor(user: UserProps) {
        this.email = user.email
        this.password = user.password
    }
}
