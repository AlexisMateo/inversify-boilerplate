import Employee from "../../api/models/Employee";

export default interface IEmployeeService{
    GetAllEmployee():Promise<Array<Employee>>;
}