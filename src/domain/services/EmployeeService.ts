import {injectable, inject} from 'inversify';
import "reflect-metadata";

import IEmployeeService from "./IEmployeeService";
import IEmployeeRepository from "../../infrastructure/repositories/IEmployeeRepository";
import Employee from "../../api/models/Employee";
import {getCustomRepository} from "typeorm";
import EmployeeRepository from "../../infrastructure/repositories/EmployeeRepository";

@injectable()
export default class EmployeeService implements IEmployeeService {

    @inject("IEmployeeRepository") 
    employeeRepository!: IEmployeeRepository;

    public EmployeeService(){}

    async GetAllEmployee():Promise<Array<Employee>>{
        const userRepository = getCustomRepository(EmployeeRepository);

        return await userRepository.GetAllEmployee();
    }

} 