import Employee from "../../api/models/Employee";

export default interface IUserService{
    GetNames():Array<string>;
}