import {injectable, inject} from 'inversify';
import "reflect-metadata";

import IEmployeeService from "./IEmployeeService";
import IEmployeeRepository from "../../infrastructure/repositories/IEmployeeRepository";
import Employee from "../../api/models/Employee";
import {getCustomRepository} from "typeorm";
import EmployeeRepository from "../../infrastructure/repositories/EmployeeRepository";
import IUserService from './user.service.interface';
import { Service } from 'typedi';
import { InjectRepository } from 'typeorm-typedi-extensions';
import UserRepository from '../../infrastructure/repositories/user.repository';

@Service()
export default class UserService implements IUserService {

    constructor(
        @InjectRepository()
        private readonly userRepository: UserRepository,
    ) {}

    
    GetNames(): string[] {
        return this.userRepository.GetNames();
    }
} 