import { createConnection, ConnectionOptions, useContainer } from 'typeorm'
import path from 'path'
import {Container} from "typedi";

const connectionConfig: ConnectionOptions = {
    name: 'default',
    type: 'mysql',
    host: "localhost",
    username: "root",
    password:"admin",
    database: "mobile-store",
    insecureAuth:true,
    port:3306,
    ssl:true,
    logging: ['query', 'error'],
    extra: { "insecureAuth": true },
    entities: [path.join(__dirname, '/domain/entities/*')]
}

useContainer(Container);
createConnection(connectionConfig);
