import Employee from "../../api/models/Employee";

export default interface IEmployeeRepository{
    GetAllEmployee():Promise<Array<Employee>>;
}