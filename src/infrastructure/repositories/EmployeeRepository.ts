import IEmployeeRepository from "./IEmployeeRepository";
import Employee from "../../api/models/Employee";
import { injectable } from "inversify";
import "reflect-metadata";


@injectable()

export default class EmployeeRepository implements IEmployeeRepository{

    
    async GetAllEmployee():Promise<Array<Employee>>{

        // var rr = await this.find({});
        
        var employees:Array<Employee> = new Array<Employee>(<Employee>{
             Name:"Alexis",
             LastName:"Mateo",
             Age:47,
             Id:1
         },<Employee>{
             Name:"Alexis",
             LastName:"Mateo",
             Age:47,
             Id:1
         },<Employee>{
             Name:"Alexis",
             LastName:"Mateo",
             Age:47,
             Id:1
         },<Employee>{
             Name:"Alexis",
             LastName:"Mateo",
             Age:47,
             Id:1
         });
 
      
         return employees;
     }

}