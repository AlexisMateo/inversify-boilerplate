
import IUserRepository from "./IUserRepository";
import { Connection } from "typeorm";
import {InjectConnection} from "typeorm-typedi-extensions";
import { Service } from "typedi";

@Service()
export default class UserRepository implements IUserRepository{

    constructor(@InjectConnection() private connection: Connection) {
    }
    
    GetNames(): string[] {
        let names: Array<string> = ['Apple', 'Orange', 'Banana'];

        return names;
    }
}