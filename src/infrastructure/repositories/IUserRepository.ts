import Employee from "../../api/models/Employee";

export default interface IUserRepository{
    GetNames():Array<string>;
}