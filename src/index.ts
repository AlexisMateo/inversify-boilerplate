import startup from'./startup';

const PORT = process.env.PORT || 8085;

startup.listen(PORT,(err:Error)=>{
    if(err){
        console.log(err);
    }
    console.log(`Server Runnings on: http://localhost:${PORT}/api/v1/employees`);
});