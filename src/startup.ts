import express from "express";
import bodyParser from "body-parser";
import Routes  from "./configuration/Routes";
import {    CreateDatabaseConnection  } from "./infrastructure/databases/mysql/MysqlConnection";

class App {

    public app: express.Application;

    constructor() {
        this.app = express();
        this.config();   
        this.registerRoutes();
        this.databaseConection();
    }

    private config(): void{
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
    }

    private registerRoutes():void{
        this.app.use('/api/v1/employees',Routes.EmployeeRoutes);
    }

    private async databaseConection():Promise<void>{

        try {
            var connection = await CreateDatabaseConnection();
            console.log("connected to mysql");

        } catch (error) {
            console.log(error)
        }
    }

}

export default new App().app;