import { inject, injectable, Container } from "inversify";
import "reflect-metadata";
import { Request, Response } from 'express';
import IEmployeeService from "../../domain/services/IEmployeeService";
import Employee from "../models/Employee";
import InversifyConfig from '../../configuration/InversifyConfig';

@injectable()
export default class EmployeeController{

    @inject("IEmployeeService") 
    employeeService!: IEmployeeService;
    
    public EmployeeController(){}

    public async GetEmployees(request:Request,response:Response){

        let employees:Array<Employee> = await InversifyConfig.Init().get<IEmployeeService>("IEmployeeService").GetAllEmployee();

        // let employees:Array<Employee> = this.employeeService.GetAllEmployee();
        
        response.status(200).send(employees);
    }
}