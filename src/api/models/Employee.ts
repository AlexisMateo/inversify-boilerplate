export default interface Employee{
     Name:string;
     LastName:string;
     Age:number;
     Id:number;
}